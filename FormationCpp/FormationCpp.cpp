#include <iostream>

#ifdef WIN32
#include <Windows.h>
#endif

#include "include/Variables.hpp"
#include "include/Tableaux.hpp"
#include "include/EntreeSortie.hpp"
#include "include/Classes.hpp"
#include "include/Templates.hpp"

using namespace std;
using namespace formationcpp;

int main()
{

#ifdef WIN32
    // �a c'est pour que Visual Studio m'affiche les caract�res Unicode...
    UINT oldcodepage = GetConsoleOutputCP();
    SetConsoleOutputCP(65001);
#endif

    // VARIABLES
    //variables::exo0();
    //variables::exo1();
    //variables::exo2();

    // TABLEAUX
    //tableaux::exo0();
    //tableaux::exo1();

    // ENTREE/SORTIE
    //entreeSortie::exo0();
    //entreeSortie::exo1();
    //entreeSortie::exo2();
    //entreeSortie::exo3();

    // POO
    //classes::exo0();
    //classes::exo1();
    //classes::exo2();
    classes::exo3();

    // TEMPLATES
    //templates::exo1();

#ifdef WIN32
    // On reset le code de la console.
    SetConsoleOutputCP(oldcodepage);
#endif
}