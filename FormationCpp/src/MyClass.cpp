#include <iostream>

#include "../include/MyClass.hpp"

using namespace formationcpp;

int MyClass::counter = 0;

MyClass::MyClass() : MyClass(0)
{
}

MyClass::MyClass(int myInt) : _myInt{myInt}, _size{myInt}
{
    _myArray = new float[_size];
    for (int i = 0; i < _size; ++i)
        _myArray[i] = (float)i;
    counter++;
}

MyClass::MyClass(const MyClass& other) : _myInt{other._myInt}, _size{other._size}
{
    _myArray = new float[_size];
    for (int i = 0; i < _size; ++i)
        _myArray[i] = other._myArray[i];
    counter++;
}

MyClass::~MyClass()
{
    counter--;
    delete[] _myArray;
}

void MyClass::publicMethod()
{
    std::cout << u8"Je suis une m�thode publique !" << std::endl;
}

void MyClass::protectedMethod()
{
    std::cout << u8"Je suis une m�thode prot�g�e !" << std::endl;
}

const int& MyClass::getInt() const
{
    return _myInt;
}

void MyClass::setInt(const int& myInt)
{
    _myInt = myInt;
}