#pragma once

#include <cstdlib>

#include <vector>
#include <array>
#include <list>

#include "Utils.hpp"

namespace formationcpp::tableaux
{

    /**
     * @brief Tableaux statiques et dynamiques.
    */
    void exo0()
    {
        float static_array1[10];

        for (int i = 0; i < 10; ++i)
            static_array1[i] = i;

        float static_array2[4] = { 0.f, 1.f, 2.f, 3.f };

        float* dynamic_array1 = (float*)malloc(5 * sizeof(float)); // C-style. D�conseill� sauf pour des libs compatibles avec du C.

        float* dynamic_array2 = new float[5]; // C++-style. � pr�f�rer.

        float* dynamic_array3 = new float[3] { 1, 2, 3 }; // Vous pouvez initialisez les valeurs d�s le d�part.

        // On oublie pas de lib�rer la m�moire !

        free(dynamic_array1);

        delete[] dynamic_array2;

        delete[] dynamic_array3;
    }

    /**
     * @brief Classes de la STL. (vectors, lists, arrays)
    */
    void exo1()
    {
        // Tableaux � taille fixe.
        std::array<char, 4> arr = { 'a', 'b', 'c' };

        // Tableaux � taille dynamique.
        std::vector<int> vec;
        vec.push_back(5);
        vec.push_back(7);
        vec.push_back(8);

        for (int i = 0; i < vec.size(); ++i)
            print("vec[", i, "] = ", vec[i]);

        // Liste doublement cha�n�e.
        std::list<std::string> lst;
        lst.push_back(u8"arri�re");
        lst.push_front(u8"milieu");
        lst.push_front(u8"d�but");

        std::cout << "l = { ";
        for (std::string& str : lst)
            std::cout << "\"" << str << "\"" << ", ";
        std::cout << "}" << std::endl;
    }
}