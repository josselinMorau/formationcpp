#pragma once
#include "Utils.hpp"

using namespace std;

namespace formationcpp::variables
{
    /**
     * @brief Montre les 2 style d'initialisation.
     * Sinon cette fonction ne sert � rien.
    */
    void exo0()
    {
        int co = 1; // C-style.

        int nga(2); // C++-style.
        // Honn�tement je vois beaucoup plus passer le 1er.
    }

    /**
     * @brief Montre le fonctionnement des variables, des r�f�rences et des pointeurs.
    */
    void exo1()
    {
        print("On initialise x.");
        // On initialise la variable x.
        int x = 16;

        print("x = ", x);

        print(u8"On initialise y comme �tant une r�f�rence vers x.");
        // Ici y est une r�f�rence � x.
        int& y = x;

        print("y = ", y);
        print("Si on change la valeur de y...");
        y = 10;
        print("y = ", y);
        print("x = ", x);
        print(u8"La valeur de x a aussi chang�e !");

        int* ptr_x = &x;
        print("ptr_x = ", ptr_x);
        print("*ptr_x = ", *ptr_x);
        print(u8"Si on change la valeur point�e par ptr_x...");
        *ptr_x = 18;
        print("*ptr_x = ", *ptr_x);
        print("x = ", x);
        print("La valeur de x change aussi !");
    }

    /**
     * @brief On peut avoir des r�f�rences de pointeur.
    */
    void exo2()
    {
        int x = 1;
        int y = 2;

        int* ptr_x = &x;
        int* ptr_y = &y;
        int*& my_ptr = ptr_x;

        print("*ptr_x = ", *ptr_x);
        print("*ptr_y = ", *ptr_y);
        print("*my_ptr = ", *my_ptr);
        print(u8"En changeant la r�f�rence de my_ptr.");
        my_ptr = ptr_y;
        print("*my_ptr = ", *my_ptr);
        print("*ptr_x = ", *ptr_x);
        print("*ptr_y = ", *ptr_y);
        print("On a chang� la valeur vers laquelle pointait ptr_x !");
    }
}