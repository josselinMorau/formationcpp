#pragma once

#include <string>

namespace formationcpp
{
    class Animal
    {
    public:
        Animal(const std::string& nom) : _nom{nom}
        {}

        const std::string& nom() const
        {
            return _nom;
        }

        virtual void crier() const = 0;

    protected:

        std::string _nom;
    };
}