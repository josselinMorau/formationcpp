#pragma once

#include "Utils.hpp"

namespace formationcpp::templates
{
    template<typename T>
    T genericAdd(T left, T right)
    {
        return left + right;
    }

    template<typename T>
    class GenericContainer
    {
    public:
        GenericContainer(size_t size) : _size{ size }
        {
            _array = new T[size];
        }

        ~GenericContainer()
        {
            delete[] _array;
        }

        size_t size() const
        {
            return _size;
        }
    private:
        size_t _size;
        T* _array;
    };

    void exo0()
    {
        GenericContainer<int> container = GenericContainer<int>(7);
        GenericContainer<std::string> containerString = GenericContainer<std::string>(3);
    }

    void exo1()
    {
        int x = genericAdd(10, 5);

        std::cout << "10 + 5 = " << x << std::endl;

        Complex c1 = Complex(10.f);
        Complex c2 = Complex(15.f, 2.f);

        Complex res = genericAdd(c1, c2);

        std::cout << "10 + (15 + 2i) = " << res << std::endl;
    }

    
}