#pragma once

#include <iostream>
#include <exception>
#include <string>

namespace formationcpp
{
    void print()
    {
        std::cout << std::endl;
    }

    template <typename T, typename... Args>
    void print(const T& t, const Args&... args)
    {
        std::cout << t;
        print(args...);
    }

    void exampleTryCatch()
    {
        try
        {
            // ...
            throw 10;

            // ...
            throw std::exception("Une erreur est survenue !");
        }
        catch (const std::exception& e)
        {
            // Gestion de l'erreur si exception envoy�e
        }
        catch (const int& i)
        {
            // Gestion de l'erreur si int envoy�
        }
    }

    class Complex
    {
    public:

        Complex() : Complex(0.f)
        {}

        Complex(float reel) : Complex(reel, 0.f)
        {}

        Complex(float reel, float im) : reel{reel}, im{im}
        {}

        std::string toString() const
        {
            return std::to_string(reel) + " + " + std::to_string(im) + "i";
        }

        operator float()
        {
            return reel;
        }

        float reel;
        float im;
    };

    Complex operator*(const Complex& l, const Complex& r)
    {
        return Complex(
            l.reel * r.reel - l.im * r.im,
            l.reel * r.im + l.im * r.reel
        );
    }

    Complex operator+(const Complex& l, const Complex& r)
    {
        return Complex(l.reel + r.reel, l.im + r.im);
    }

    Complex operator-(const Complex& l, const Complex& r)
    {
        return Complex(l.reel - r.reel, l.im - r.im);
    }

    bool operator==(const Complex& l, const Complex& r)
    {
        return l.reel == r.reel && l.im == r.im;
    }

    std::ostream& operator<<(std::ostream& os, const Complex& complex)
    {
        return os << complex.toString();
    }
}