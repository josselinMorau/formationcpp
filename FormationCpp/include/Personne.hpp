#pragma once

#include <string>
#include <iostream>

namespace formationcpp
{

    class Personne
    {
    public:

        Personne(const std::string& nom, int age) :
            age{age},
            nom{nom}
        {}

        ~Personne()
        {
            std::cout << "Adieu " << nom << " ! " << std::endl;
        }

        int age;
        std::string nom;
    };
}