#pragma once

namespace formationcpp
{
    struct MyStruct
    {
        int myInt;

        float myFloat;

        MyStruct(int myInt, float myFloat) :
            myInt{myInt}, myFloat{myFloat}
        {}
    };
}