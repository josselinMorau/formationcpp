#pragma once

#include <iostream>
#include <fstream>
#include <string>
#include <cstring>

namespace formationcpp::entreeSortie
{
    /**
     * @brief �crire dans la console.
    */
    void exo0()
    {
        // Affichage dans la console.
        std::cout << "x = " << 10 << std::endl;

        // Si on veut afficher des bool�ens avec "true" et "false".
        std::cout << std::boolalpha << true << " " << false << std::endl;
    }

    /**
     * @brief Lire dans la console.
    */
    void exo1()
    {
        // Lire les entr�es de la console.
        std::cout << u8"�crivez ce que vous voulez..." << std::endl;
        std::string entree;
        std::cin >> entree;
        std::cout << u8"Vous venez d'�crire : \"" << entree << "\"" << std::endl; // On remarquera que entree s'arr�te au 1er espace donn�

        // On appelle un getline avant pour ne pas r�cuperer les restes de l'entr�e pr�c�dente.
        std::getline(std::cin, entree);

        // Lire une ligne enti�re
        std::cout << u8"�crivez ce que vous voulez encore..." << std::endl;
        std::getline(std::cin, entree);
        std::cout << u8"Vous venez d'�crire : \"" << entree << "\"" << std::endl; // L� on a toute la ligne.

        // Lire un entier.
        std::cout << u8"Entrez un entier..." << std::endl;
        int x = 0;
        std::cin >> x;
        std::cout << "x = " << x << std::endl;
    }

    /**
     * @brief Lire dans un fichier.
    */
    void exo2()
    {
        const std::string path = "./resources/Paroles.txt";
        std::ifstream ifs = std::ifstream(path);

        if (ifs.good())
        {
            int nbLines = 0;
            int count = 0;

            std::cout << u8"Combien de lignes voulez-vous lire ?" << std::endl;
            std::cin >> nbLines;

            // Cas o� l'entier est n�gatif.
            if (nbLines < 0)
                nbLines = 0;

            std::string line;
            while (!ifs.eof() && count < nbLines)
            {
                std::getline(ifs, line);
                std::cout << "[" << count << "] : " << line << std::endl;
                count++;
            }
        }

        // On ferme le fichier. Le fichier est ferm� automatique � la fin du bloc de code courant.
        ifs.close();
    }

    /**
     * @brief �crire dans un fichier.
    */
    void exo3()
    {
        const std::string path = "./resources/out.txt";
        std::ofstream ofs = std::ofstream(path);

        if (ofs)
        {
            int nbLines = 5;

            std::cout << u8"Combien de lignes voulez-vous �crire ?" << std::endl;
            std::cin >> nbLines;

            for (int i = 0; i < nbLines; ++i)
            {
                ofs << "Ligne n� " << i + 1 << std::endl;
            }
        }
        ofs.close();
    }
}