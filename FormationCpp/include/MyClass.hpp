#pragma once

namespace formationcpp
{
    class MyClass
    {
    public:

        /**
         * @brief Constructeur par d�faut.
        */
        MyClass();

        /**
         * @brief Constructeur de copie.
         * @param other 
        */
        MyClass(const MyClass& other);

        /**
         * @brief Constructeur avec argument.
         * @param myInt 
        */
        MyClass(int myInt);

        /*
        * @brief Destructeur.
        */
        ~MyClass();

        /**
         * @brief Une m�thode publique.
        */
        void publicMethod();

        /**
         * @brief Un getter.
         * @return 
        */
        const int& getInt() const;

        /**
         * @brief Un setter.
         * @param myInt 
        */
        void setInt(const int& myInt);

        static int counter;

    protected:

        /**
         * @brief Une m�thode priv�e.
        */
        void protectedMethod();

    private:

        int _myInt;

        int _size;

        float* _myArray;
    };
}