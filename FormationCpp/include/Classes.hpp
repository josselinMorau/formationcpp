#pragma once
#include <iostream>
#include <vector>

#include "MyClass.hpp"
#include "MyStruct.hpp"
#include "Animal.hpp"
#include "Oiseaux.hpp"
#include "Personne.hpp"

namespace formationcpp::classes
{
    void ex()
    {
        const Oiseau oiseau = Oiseau("Piafabec");
    }

    /**
     * @brief Les 2 fa�ons d'initialiser une struct.
    */
    void exo0()
    {
        //MyStruct myStruct1 = { 
        //    10, //myInt
        //    20.f // myFloat
        //};

        //MyStruct myStruct2 = MyStruct(10, 20.0f);

        Personne tom = Personne("Tom", 27);

        Personne* billy = new Personne("Billy", 12);

        // Si on ne d�truit pas nous m�me Billy, il va se perdre dans la m�moire !!!
        //delete billy; 

        // La fonction est termin�e et donc on sait qu'on n'a plus besoin de Tom.
    }

    /**
     * @brief Expliquons l'h�ritage et le polymorphisme.
    */
    void exo1()
    {
        std::vector<Oiseau> oiseaux;
        oiseaux.push_back(Canard());
        oiseaux.push_back(Oiseau());
        oiseaux.push_back(Pingouin("Roger"));

        for (const Oiseau& oiseau : oiseaux)
        {
            std::cout << "Le nom de l'animal est : " << oiseau.nom() << std::endl;
        }
    }


    /**
     * @brief Expliquons l'h�ritage et le polymorphisme Ep.2 L'enfant errant...
    */
    void exo2()
    {
        std::vector<Oiseau> oiseaux = {
            Canard(),
            Coq(),
            Pingouin()
        };

        for (const Oiseau& oiseau : oiseaux)
        {
            std::cout << "Le " << oiseau.nom() << " fait ";
            oiseau.crier();
        }
    }

    /**
     * @brief Expliquons l'h�ritage et le polymorphisme 3 & Knuckles
    */
    void exo3()
    {
        std::vector<Animal*> animaux = {
            new Canard(),
            new Coq(),
            new Pingouin()
        };

        for (Animal* animal : animaux)
        {
            std::cout << "Le " << animal->nom() << " fait ";
            animal->crier();
        }

        // Il ne faut pas oublier le delete � la fin !
        for (Animal* animal : animaux)
            delete animal;
    }
}