﻿#pragma once

#include <iostream>

#include "Animal.hpp"

namespace formationcpp
{

    class Oiseau : public Animal
    {
    public:

        Oiseau() : Oiseau("Oiseau")
        {}

        Oiseau(const std::string& nom) : Animal(nom)
        {}

        virtual void crier() const override
        {
            std::cout << "Piou ?" << std::endl;
        }

        void mangerDesVers()
        {
            //
        }
    };


    class Canard : public Oiseau
    {
    public:

        Canard() : Canard("Canard")
        {}

        Canard(const std::string& nom) : Oiseau(nom)
        {}

        void crier() const override final
        {
            std::cout << "Coin !" << std::endl;
        }
    };

    class Coq : public Oiseau
    {
    public:

        Coq() : Coq("Coq")
        {}

        Coq(const std::string& nom) : Oiseau(nom)
        {}

        void crier() const override final
        {
            std::cout << "Cocorico !" << std::endl;
        }
    };

    class Pingouin : public Oiseau
    {
    public:

        Pingouin() : Pingouin("Pingouin")
        {}

        Pingouin(const std::string& nom) : Oiseau(nom)
        {}

        virtual void crier() const override
        {
            std::cout << u8"(ˇò_ó)9" << std::endl;
        }
    };
}